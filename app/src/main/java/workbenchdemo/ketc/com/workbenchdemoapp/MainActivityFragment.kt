package workbenchdemo.ketc.com.workbenchdemoapp

import android.support.v4.app.Fragment
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.serori.androidutils.listview.adapter.DeletableListAdapter
import com.serori.androidutils.listview.model.SimpleText
import kotlinx.android.synthetic.main.fragment_main.view.*
import java.util.*

/**
 * A placeholder fragment containing a simple view.
 */
class MainActivityFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        val view: View = inflater!!.inflate(R.layout.fragment_main, container, false)
        val adapter = DeletableListAdapter(context, ArrayList<SimpleText>(), true)
        adapter.add(SimpleTextImpl("hoge1"))
        adapter.add(SimpleTextImpl("hoge2"))
        adapter.add(SimpleTextImpl("hoge3"))
        adapter.add(SimpleTextImpl("hoge4"))
        adapter.add(SimpleTextImpl("hoge5"))
        adapter.add(SimpleTextImpl("hoge6"))
        adapter.add(SimpleTextImpl("hoge7"))
        adapter.add(SimpleTextImpl("hoge8"))
        adapter.add(SimpleTextImpl("hoge9"))
        adapter.add(SimpleTextImpl("hoge10"))
        adapter.add(SimpleTextImpl("hoge11"))
        adapter.add(SimpleTextImpl("hoge12"))
        adapter.onClickRemoveButtonListener = {
            Toast.makeText(context, "onClickDeleteButton ${it.text}", Toast.LENGTH_SHORT).show()
        }
        view.dragndroppable_list.setOnStartDragListener { Toast.makeText(context, "onStartDrag", Toast.LENGTH_SHORT).show() }
        view.dragndroppable_list.setOnDragCancelListener { Toast.makeText(context, "onDragCancel", Toast.LENGTH_SHORT).show() }
        view.dragndroppable_list.setOnDropListener { i1, i2 ->
            Toast.makeText(context, "onDrop draggingItem:${adapter.getItem(i1).text},droppedItem:${adapter.getItem(i2).text}", Toast.LENGTH_SHORT).show()
        }
        view.dragndroppable_list.adapter = adapter
        return view
    }
}

private data class SimpleTextImpl(override var text: String) : SimpleText